struct PSTHPlotArgs <: DPHT.DPHPlotArgs
    preserve_location::Bool
    color::NTuple{3,FixedPointNumbers.Normed{UInt8,8}}
end

abstract type AbstractAligned end;
struct CueAligned <: AbstractAligned end;
struct SaccadeAligned <: AbstractAligned end;
struct TargetAligned <: AbstractAligned end;
struct DistractorAligned <: AbstractAligned end;
struct LeftFixationAligned <: AbstractAligned end;
struct FixationAligned <: AbstractAligned end;
struct RewardAligned <: AbstractAligned end;
struct FailureAligned <: AbstractAligned end;

struct StimulusAligned <: AbstractAligned
	idx::Int64
end

shortname(::Type{TargetAligned}) = "target"
shortname(::Type{DistractorAligned}) = "distractor"
shortname(::Type{CueAligned}) = "cue"
shortname(::Type{SaccadeAligned}) = "saccade"
shortname(::Type{LeftFixationAligned}) = "left_fixation"
shortname(::Type{FixationAligned}) = "fixation"
shortname(::Type{StimulusAligned}) = "stimulus"
shortname(X::StimulusAligned) = "$(shortname(StimulusAligned))$(X.idx)"
shortname(::Type{RewardAligned}) = "reward"
shortname(::Type{FailureAligned}) = "failure"

get_alignment(trials::Vector{Stimulus.Trial}, ::Type{CueAligned}) = :response
get_alignment(trials::Vector{Stimulus.NewTrial}, ::Type{CueAligned}) = :response_on
get_alignment(trials::Vector{Stimulus.NewTrial}, ::Type{TargetAligned}) = :target
get_alignment(trials::Vector{Stimulus.NewTrial}, ::Type{DistractorAligned}) = :distractor
get_alignment(trials::Vector{Stimulus.NewTrial}, alignment::FixationAligned) = :fix_start
get_alignment(trials::Vector{T}, ::Type{SaccadeAligned}) where T <: Stimulus.AbstractTrial = :saccade
get_alignment(trials::Vector{Stimulus.Trial}, ::Type{TargetAligned}) = :target
get_alignment(trials::Vector{Stimulus.Trial}, ::Type{DistractorAligned}) = :distractors
get_alignment(trials::Vector{Stimulus.NewTrial}, ::Type{LeftFixationAligned}) = :left_fixation
get_alignment(trials::Vector{Stimulus.NewTrial}, alignment::StimulusAligned) = Symbol(shortname(alignment))
function get_alignment(trials::Vector{Stimulus.Trial}, alignment::StimulusAligned)
	if alignment.idx == 1
		return :target
	elseif alignment.idx == 2
		return :distractors
	else
		error("Unknown alignment $alignment")
	end
end
get_alignment(trials::Vector{Stimulus.Trial}, ::Type{RewardAligned}) = :reward
get_alignment(trials::Vector{Stimulus.NewTrial}, ::Type{RewardAligned}) = :reward_on
get_alignment(trials::Vector{Stimulus.Trial}, ::Type{FailureAligned}) = :failure
get_alignment(trials::Vector{Stimulus.NewTrial}, ::Type{FailureAligned}) = :failure

alignment_map = Dict(:target => TargetAligned,
                     :distractor => DistractorAligned,
                     :saccade => SaccadeAligned,
                     :response => CueAligned,
                     :response_on => CueAligned,
                     :cue => CueAligned,
                     :left_fixation => LeftFixationAligned,
					 :fixation => FixationAligned(),
					 :stimulus1 => StimulusAligned(1),
					 :stimulus2 => StimulusAligned(2),
					 :stimulus3 => StimulusAligned(3),
					 :reward => RewardAligned,
					 :failure => FailureAligned)

TrialsAnalysis.get_trials(::Type{AbortedSaccadeTrial}) = get_aborted_trials()

"""
Return trials of the specified type
"""
function TrialsAnalysis.get_trials(::T) where T <: AbstractTrialType
    error("Not implemented")
end

function TrialsAnalysis.get_trials(::Type{Stimulus.NewTrial})
    trials = cd(DPHT.process_level("day")) do
        trials = TrialsAnalysis.Trials(TrialsAnalysis.TrialsArgs())
    end
    trials
end

function TrialsAnalysis.get_trials(::Type{Stimulus.Trial})
    trials = cd(DPHT.process_level("session")) do
        trials = Stimulus.loadTrialInfo("event_data.mat")
    end
    labels = Stimulus.get_location_label(trials, :target)
    trials, 1:length(trials), labels
end

function TrialsAnalysis.get_trials(::Type{T}) where T <: AbstractTrialType
    trials = TrialsAnalysis.get_trials()
    TrialsAnalysis.get_trials(T, trials.trials)
end

function TrialsAnalysis.get_trials(::Type{T}) where T <: TrialsAnalysis.AbstractIncorrectOnStimTrial
    trials = TrialsAnalysis.get_trials()
    settings, etrials = cd(DPHT.process_level("session")) do
                      settings = ExperimentSettings(ExperimentSettingsArgs())
                      etrials = get_eyelinktrials(EyelinkTrialsArgs(Stimulus.NewEyelinkTrial))
                      settings, etrials
                end
    eidx = findall(trial->istrialtype(trial, T, settings),etrials.trials)
    trials[eidx], eidx
end

function TrialsAnalysis.get_trials(::Type{T}) where T <: Union{TrialsAnalysis.CorrectAfterCorrect, TrialsAnalysis.CorrectBeforeCorrect}
    trials = TrialsAnalysis.get_trials()
    cidx = Int64[]
    for i in 2:length(trials)
        trial0 = trials[i-1]
        trial1 = trials[i]
        if ((TrialsAnalysis.get_reward_start(trial0) > 0.0) && (TrialsAnalysis.get_reward_start(trial1) > 0.0)) == false
            continue
        end
        if T <: TrialsAnalysis.CorrectAfterCorrect
            push!(cidx, i)
        else
            push!(cidx, i-1)
        end
    end
    ctrials = trials[cidx]
    ctrials, cidx
end

function TrialsAnalysis.get_trials(::Type{T}, trials::Vector{T2}, args...) where T <: AbstractTrialType where T2 <: Stimulus.AbstractTrial
    idx = findall(t->TrialsAnalysis.istrialtype(t,T,args...), trials)
    ctrials = trials[idx]
    ctrials, idx
end

function TrialsAnalysis.get_trials(::Type{T}, ::Type{T2}) where T <: AbstractTrialType where T2 <: TrialsAnalysis.AbstractTrials
    trials = cd(DPHT.process_level(T2)) do
        TrialsAnalysis.get_trials(fieldtype(T2,:args)())
    end
    TrialsAnalysis.get_trials(T, trials.trials)
end

function get_ntrials(eyelinkdata::Eyelink.EyelinkData)
    get_ntrials(eyelinkdata.events)
end

function get_ntrials(events::Vector{Eyelink.Event})
    nt = 0
    for ee in events
        msg = replace(ee.message, " " => "")
        if msg == Stimulus.trialevents[:trial_start]
            nt += 1
        end
    end
    nt
end
