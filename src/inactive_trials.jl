import TrialsAnalysis: NoResponseTrial

struct InactiveTrialsArgs <: DPHT.DPHDataArgs
    min_post_cue_time::Float64
end

struct InactiveTrials <: DPHT.DPHData
    midx::Vector{Int64}
    args::InactiveTrialsArgs
end

DPHT.filename(::Type{InactiveTrials}) = "inactive_trials.mat"
DPHT.level(::Type{InactiveTrials}) = "session"
DPHT.datatype(::Type{InactiveTrialsArgs}) = InactiveTrials

"""
Get the trials for which the monkey failed to make a response to the current session
"""
function get_inactive_trials(;min_post_cuetime=0.5)
    trials = TrialsAnalysis.OldTrials(TrialsAnalysis.OldTrialsArgs())
    eyetrials = get_eyelinktrials(EyelinkTrialsArgs())
    aa = DPHT.get_level_name("subject")
    if aa == "Pancake"
        fixsize = div(100,2)
        grid = Stimulus.Grid(1440,900, 5,5)
    elseif aa == "James"
        fixsize = div(200,2)
        grid = Stimulus.Grid(1920,1200, 5,5)
    else
        error("Unknown subject")
    end
    get_inactive_trials(eyetrials.trials, trials.trials, grid, fixsize;min_post_cuetime=min_post_cuetime)
end

function get_inactive_trials(eyetrials::Vector{Stimulus.EyelinkTrial}, trials::Vector{Stimulus.Trial}, grid::Stimulus.Grid, fixsize::Real;min_post_cuetime=0.5)
    rtrials = Stimulus.getTrialType(trials, :response)
    ctrials = Stimulus.getTrialType(trials, :reward)
    mtrials = setdiff(rtrials, ctrials)
    midx = findall(in(mtrials), trials)
    Δt = fill(Inf, length(midx))
    for (ii,mm) in enumerate(midx)
        etrial = eyetrials[mm]
        fidx = searchsortedfirst([s.start_time for s in etrial.saccades], etrial.response)
        if 0 < fidx <= length(etrial.saccades) 
            Δt[ii] = etrial.saccades[fidx].start_time - etrial.response
        end
    end
    midx[Δt .> min_post_cuetime]
end

function get_inactive_trials(args::InactiveTrialsArgs;force_redo=false, do_save=true)
    fname = DPHT.filename(args)
    redo = force_redo || !DPHT.computed(args)
    if !redo
        X = DPHT.load(args)
    else
        midx = get_inactive_trials(;min_post_cuetime=args.min_post_cue_time)
        X = InactiveTrials(midx, args)
        if do_save
            DPHT.save(X)
        end
    end
    X
end

function TrialsAnalysis.get_trials(trialtype::NoSaccadeTrial)
    args = InactiveTrialsArgs(trialtype::tmin)
    X = get_inactive_trials(args)
    mtrials, midx = cd(DPHT.process_level("session")) do
        trials = Stimulus.loadTrialInfo("event_data.mat")
        trials[X.midx], X.midx
    end
    mtrials, midx
end

function TrialsAnalysis.get_trials(::Type{NoSaccadeTrial}, trials::Vector{T};tmin=300.0) where T <: Stimulus.AbstractTrial
    args = InactiveTrialsArgs(tmin)
    X = get_inactive_trials(args)
    trials[X.midx], X.midx
end

function get_cue_time()
end
