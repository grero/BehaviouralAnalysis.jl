import Base:isapprox
import StableHashes:shash

abstract type AbstractTimeReference end
struct EyelinkTimeReference <: AbstractTimeReference end
struct PlexonTimeReference <: AbstractTimeReference end

abstract type AbstractSaccadeType end
abstract type AbstractTrialSaccadeType <: AbstractSaccadeType end
abstract type AbstractResponseSaccade <: AbstractTrialSaccadeType end
struct SpontaneousSaccade <: AbstractSaccadeType end
struct ResponseSaccade <: AbstractResponseSaccade end
struct CorrectSaccade <: AbstractResponseSaccade end
struct IncorrectSaccade <: AbstractResponseSaccade end
struct AbortedSaccade <: AbstractTrialSaccadeType end
struct DistractedSaccade <: AbstractResponseSaccade end
struct PrematureSaccade <: AbstractTrialSaccadeType end

abstract type EventTimeRef end

struct PreviousTime <: EventTimeRef
    time::Float64
end

struct NextTime <: EventTimeRef
    time::Float64
end

#Base.:(<)(x::Real, y::T) where T <: EventTimeRef = Base.<(convert(Float64,x), y.time)
#Base.:(>)(x::Real, y::T) where T <: EventTimeRef = Base.>(convert(Float64,x), y.time)

Base.Symbol(::Type{SpontaneousSaccade}) = :spontaneoussaccade
Base.Symbol(::Type{CorrectSaccade}) = :correctsaccade
Base.Symbol(::Type{IncorrectSaccade}) = :incorrectsaccade
Base.Symbol(::Type{AbortedSaccade}) = :abortedsaccade
Base.Symbol(::Type{ResponseSaccade}) = :responsesaccade
Base.Symbol(::Type{DistractedSaccade}) = :distractedsaccade
Base.Symbol(::Type{PrematureSaccade}) = :prematuresaccade

get_trialtype(::Type{AbstractSaccadeType}) = error("Not implemented")
get_trialtype(::Type{CorrectSaccade}) = TrialsAnalysis.CorrectTrial
get_trialtype(::Type{IncorrectSaccade}) = TrialsAnalysis.IncorrectTrial
get_trialtype(::Type{AbortedSaccade}) = TrialsAnalysis.AbortedTrial
get_trialtype(::Type{ResponseSaccade}) = TrialsAnalysis.ResponseTrial
get_trialtype(::Type{DistractedSaccade}) = TrialsAnalysis.DistractedTrial 
get_trialtype(::Type{PrematureSaccade}) = TrialsAnalysis.JumpTheGunTrial

function get_saccade_type(X::Symbol)
    if X == :correctsaccadetrial
        return CorrectSaccade
    elseif X == :abortedsaccadetrial
        return AbortedSaccade
    elseif X == :incorrectsaccadetrial
        return IncorrectSaccade
	elseif X == :responsesaccadetrial
		return ResponseSaccade
    elseif X == :distractedsaccadetrial
        return DistractedSaccade
	elseif X == :jumptheguntrial
		return PrematureSaccade
    else
		return nothing
    end
end

struct SaccadesArgs{T<:AbstractTimeReference, T2<:AbstractSaccadeType} <: DPHT.DPHDataArgs
    timeref::T
    stype::T2
end

shash(X::EyelinkTimeReference,h::UInt64) = shash("eyelinktimereference", h)
shash(X::PlexonTimeReference,h::UInt64) = shash("plexontimereference", h)
shash(X::SpontaneousSaccade, h::UInt64) = shash("spontaneoussaccade",h)
shash(X::CorrectSaccade,h::UInt64) = shash("correctsaccade", h)
shash(X::IncorrectSaccade,h::UInt64) = shash("incorrectsaccade", h)
shash(X::AbortedSaccade, h::UInt64) = shash("abortedsaccade",h)
shash(X::DistractedSaccade, h::UInt64) = shash("distractedsaccade", h)
shash(X::PrematureSaccade, h::UInt64) = shash("prematuresaccade", h)

function Base.convert(::Type{Dict{String,Any}}, X::SaccadesArgs{T,T2}) where T <: AbstractTimeReference where T2 <: AbstractSaccadeType
    Q = Dict{String,Any}()
    if T <: EyelinkTimeReference
        Q["timeref"] = "eyelink"
    elseif T <: PlexonTimeReference
        Q["timeref"] = "plexon"
    end
    if T2 <: CorrectSaccade
        Q["stype"] = "correctsaccade"
    elseif T2 <: IncorrectSaccade
        Q["stype"] = "incorrectsaccade"
    elseif T2 <: SpontaneousSaccade
        Q["stype"] = "spontaneoussaccade"
    elseif T2 <: AbortedSaccade
        Q["stype"] = "abortedsaccade"
    elseif T2 <: DistractedSaccade
        Q["stype"] = "distractedsaccade"
    elseif T2 <: PrematureSaccade
        Q["stype"] = "prematuresaccade"
    end
    Q
end

function Base.convert(::Type{SaccadesArgs{T,T2}}, Q::Dict{String,Any}) where T <: AbstractTimeReference where T2 <: AbstractSaccadeType
    SaccadesArgs(T(), T2())
end

"""
Contains all the saccades in a session, with onset and offsets aligned to the nearest trial start
"""
struct Saccades{T<:AbstractTimeReference,T2<:AbstractSaccadeType} <: DPHT.DPHData
    onset::Vector{Float64}
    duration::Vector{Float64}
    startpos::Matrix{Float32}
    endpos::Matrix{Float32}
    angle::Vector{Float32}
    trialidx::Vector{Int64}
    args::SaccadesArgs{T,T2}
end

DPHT.filename(::Type{Saccades{T,T2}}) where T <: AbstractTimeReference where T2 <: AbstractSaccadeType = "all_saccades.mat"
DPHT.filename(::Type{Saccades{PlexonTimeReference, CorrectSaccade}}) = "correct_saccades.mat"
DPHT.filename(::Type{Saccades{PlexonTimeReference, IncorrectSaccade}}) = "incorrect_saccades.mat"
DPHT.filename(::Type{Saccades{PlexonTimeReference, SpontaneousSaccade}}) = "spontaneous_saccades.mat"
DPHT.filename(::Type{Saccades{PlexonTimeReference, AbortedSaccade}}) = "aborted_saccades.mat"
DPHT.filename(::Type{Saccades{PlexonTimeReference, DistractedSaccade}}) = "distracted_saccades.mat"
DPHT.filename(::Type{Saccades{PlexonTimeReference, PrematureSaccade}}) = "premature_saccades.mat"
DPHT.level(::Type{Saccades{T,T2}}) where T <: AbstractTimeReference  where T2 <: AbstractSaccadeType = "session"
DPHT.datatype(::Type{SaccadesArgs{T,T2}}) where T <: AbstractTimeReference where T2 <: AbstractSaccadeType = Saccades{T,T2}

function Base.convert(::Type{Saccades{EyelinkTimeReference, SpontaneousSaccade}}, events::Vector{Eyelink.Event})
    saccade_events = filter(ee->ee.eventtype == :endsacc, events)
    trial_start_events = filter(ee->replace(ee.message, " " => "") == Stimulus.trialevents[:trial_start], events)
    trial_start = [ee.sttime for ee in trial_start_events]
    saccade_start = [ee.sttime for ee in saccade_events]
    duration = [float(ee.entime) - float(ee.sttime) for ee in saccade_events]
    onset = fill(NaN, length(saccade_start))
    startpos = fill(0.0f0, 2, length(saccade_start))
    endpos = fill!(similar(startpos), 0.0f0)
    trialidx = fill(0, length(saccade_start))
    #align each saccade to the precedding trial start
    for (ii,ss) in enumerate(saccade_start)
        jj = searchsortedlast(trial_start, ss)
        if 0 < jj <= length(trial_start)
            onset[ii] = ss - trial_start[jj]
            trialidx[ii] = jj
            endpos[:,ii] = [saccade_events[ii].genx, saccade_events[ii].geny]
            startpos[:,ii] = [saccade_events[ii].gstx, saccade_events[ii].gsty]
        end
    end
    idx = findall(isfinite, onset)
    onset = onset[idx]
    duration = duration[idx]
    startpos = startpos[:, idx]
    endpos = endpos[:, idx]
    trialidx = trialidx[idx]
    _angle = atan.(endpos[2,:] - startpos[2,:], endpos[1,:] - startpos[1,:])
    Saccades(onset, duration, startpos, endpos, _angle, trialidx, SaccadesArgs(EyelinkTimeReference(), SpontaneousSaccade()))
end

function Saccades(args::SaccadesArgs{T,T2};force_redo=false, do_save=true) where T <: AbstractTimeReference where T2 <: AbstractSaccadeType
    fname = DPHT.filename(args)
    redo = force_redo || !DPHT.computed(args)
    if !redo
        X = DPHT.load(args)
    else
		if T <: PlexonTimeReference
			trials = TrialsAnalysis.get_trials()
		else
            # FIXME: This is not reflected properly
			trials = get_eyelinktrials(EyelinkTrialsArgs())
		end
        day_name = DataProcessingHierarchyTools.get_level_name("day")

        function get_trialidx(trialidx)
            # only do this if we are working in neural time
            if day_name == "20140807" && T <: PlexonTimeReference
                println("Applying trialidx hack...")
                # extra trial in the eyelink data for this day
                _idx = findall(trialidx .> 1)
                _trialidx = trialidx[_idx] .- 1
            else
                _trialidx = trialidx
                _idx = 1:length(trialidx)
            end
            return _trialidx, _idx
        end

        if T2 <: SpontaneousSaccade
            edffile = glob("*.edf")
            if isempty(edffile)
                error("No edffile found")
            end
            eyelinkdata = Eyelink.load(edffile[1];check=0, load_events=true, load_samples=false)
            n_eyelinktrials = get_ntrials(eyelinkdata)
            all_saccades = convert(Saccades{EyelinkTimeReference, T2}, eyelinkdata.events)
            onset = all_saccades.onset
            trialidx,tidx = get_trialidx(all_saccades.trialidx)
            onset = [1000.0*trials[trialidx[i]].start + onset[tidx[i]] for i in 1:length(trialidx)]
            X = Saccades(onset, all_saccades.duration[tidx], all_saccades.startpos[:,tidx], all_saccades.endpos[:,tidx], all_saccades.angle[tidx], trialidx, args)
        elseif T2 <: AbstractTrialSaccadeType
            trialidx, onset, offset, startpos, endpos = get_aborting_saccade(get_trialtype(T2))
            _angle = [atan(endpos[i][2] - startpos[i][2], endpos[i][1] - startpos[i][1]) for i in 1:length(endpos)]
            duration = offset - onset
            trialidx,tidx = get_trialidx(trialidx)
            if length(trials) < maximum(trialidx)
                #kind of hackish, but it works
				# FIXME: This assumes that we always have the same type of trials
                etrials = get_eyelinktrials(EyelinkTrialsArgs())
                _tidx = match_trials(etrials.trials, trials.trials)
                trialidx = trialidx[trialidx.>=_tidx] .- _tidx .+ 1
            end
			onset = [1000*TrialsAnalysis.get_trial_start(trials[trialidx[i]]) + 1000.0*onset[tidx[i]] for i in 1:length(trialidx)]
            X = Saccades(onset, duration[tidx],
                         convert(Matrix{Float32}, cat(startpos..., dims=2))[:,tidx],
                         convert(Matrix{Float32}, cat(endpos..., dims=2))[:,tidx],
                         convert(Vector{Float32},_angle)[tidx], trialidx, args)
        end
        ###
        if do_save
            DPHT.save(X)
        end
    end
    X
end

"""
Returns the trials associated with the supplied SaccadeType
"""
function TrialsAnalysis.get_trials(::Type{T}) where T <: AbstractSaccadeType
    sargs_c = SaccadesArgs(PlexonTimeReference(), T())
    saccades = Saccades(sargs_c)
    trials = TrialsAnalysis.get_trials()
    trials[saccades.trialidx], saccades.trialidx, saccades.angle
end
