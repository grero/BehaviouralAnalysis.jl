import TrialsAnalysis: istrialtype, ResponseTrial, CorrectTrial, IncorrectTrial, AbortedTrial, AbstractTrials, AbstractTrialsArgs, DistractedTrial, ConfusedTrial, IncorrectOnStimTrial

struct EyelinkTrialsArgs{T<:Stimulus.AbstractEyeTrial} <: AbstractTrialsArgs
	eyelinktrialtype::Type{T}
end

"""
Try and figure out what kind of eyelinktrials we are dealing with
"""
function EyelinkTrialsArgs()
	T = cd(DPHT.process_level("day")) do
		# TODO: This assumes that we have already extracted the plexon trial structure
		if isfile("event_markers.csv") || islink("event_markers.vsv")
			T = Stimulus.NewEyelinkTrial
		else
			T = Stimulus.EyelinkTrial
		end
		T
	end
	EyelinkTrialsArgs(T)
end

struct EyelinkTrials{T<:Stimulus.AbstractEyeTrial} <: AbstractTrials
    trials::Vector{T}
	args::EyelinkTrialsArgs{T}
end

DPHT.filename(::Type{EyelinkTrials{T}})  where T <: Stimulus.AbstractEyeTrial = "eyetrials.bson"
DPHT.filename(X::EyelinkTrialsArgs{T})  where T <: Stimulus.AbstractEyeTrial = DPHT.filename(EyelinkTrials{T})
DPHT.level(::Type{EyelinkTrials{T}})  where T <: Stimulus.AbstractEyeTrial = "session"
DPHT.datatype(::Type{EyelinkTrialsArgs{T}})  where T <: Stimulus.AbstractEyeTrial = EyelinkTrials{T}

function DPHT.load(args::EyelinkTrialsArgs{T}) where T <: Stimulus.AbstractEyeTrial
    fname = DPHT.filename(args)
    if islink(fname) && DPHT.git_annex != nothing
        run(`$(DPHT.git_annex()) get $(fname)`)
    end
    et = BSON.load(DPHT.filename(args), @__MODULE__)
    if :et in keys(et)
        etrials = et[:et]
    elseif :eyetrials in keys(et)
        etrials = et[:eyetrials]
    end
	eyetrials = convert(Vector{T}, etrials)
    EyelinkTrials(eyetrials, args)
end

function DPHT.save(X::EyelinkTrials{T}, fname=DPHT.filename(X.args)) where T <: Stimulus.AbstractEyeTrial
    BSON.bson(fname, Dict(:et=>X.trials))
end

function get_eyelinktrials(args::EyelinkTrialsArgs{T};do_save=true, force_redo=false) where T <: Stimulus.AbstractEyeTrial

    redo = force_redo || !DPHT.computed(args)
    if redo
        edffile = glob("*.edf")
        if isempty(edffile)
            error("No EDF file found")
        elseif islink(edffile[1]) && DPHT.git_annex != nothing
            run(`$(DPHT.git_annex()) get $(edffile[1])`)
        end
        eyelinkdata = Eyelink.load(edffile[1];check=0,load_events=true, load_samples=false)
        events = eyelinkdata.events
        eyetrials = Stimulus.parse(T, events)
		T2 = DPHT.datatype(args)
        X = T2(eyetrials, args)
        if do_save
            DPHT.save(X)
        end
    else
        X = DPHT.load(args)
    end
    X
end

istrialtype(trial::Stimulus.EyelinkTrial, ::Type{ResponseTrial}) = isfinite(trial.response)
istrialtype(trial::Stimulus.EyelinkTrial, ::Type{CorrectTrial}) = istrialtype(trial, ResponseTrial)&&isfinite(trial.reward)
istrialtype(trial::Stimulus.EyelinkTrial, ::Type{IncorrectTrial}) = istrialtype(trial, ResponseTrial)&&isfinite(trial.failure)
istrialtype(trial::Stimulus.EyelinkTrial, ::Type{AbortedTrial}) = !istrialtype(trial, ResponseTrial)&&isfinite(trial.failure)
# distractor presented but aborted before response was given
istrialtype(trial::Stimulus.EyelinkTrial, ::Type{JumpTheGunTrial}) = !isnan(trial.distractors[1].timestamp) && isnan(trial.response)

function istrialtype(trial::Stimulus.EyelinkTrial, ::Type{NoSaccadeTrial}, min_time=Inf)
    aa = istrialtype(trial, ResponseTrial)
    if aa == false
        return false
    end
    #TODO: Find a way to get the grid so that we can find the aborting saccade
end

istrialtype(trial::Stimulus.NewEyelinkTrial, ::Type{ResponseTrial}) = trial.response_on > 0.0
istrialtype(trial::Stimulus.NewEyelinkTrial, ::Type{CorrectTrial}) = istrialtype(trial, ResponseTrial)&&(trial.reward_on > 0.0)
istrialtype(trial::Stimulus.NewEyelinkTrial, ::Type{IncorrectTrial}) = istrialtype(trial, ResponseTrial)&&(trial.failure > 0.0)
istrialtype(trial::Stimulus.NewEyelinkTrial, ::Type{AbortedTrial}) = !istrialtype(trial, ResponseTrial)&&(trial.failure>0.0)
istrialtype(trial::Stimulus.NewEyelinkTrial, ::Type{JumpTheGunTrial}) = length(trial.stimulus) == 2 && trial.response_on == 0.0

function istrialtype(trial::Stimulus.NewEyelinkTrial, ::Type{NoSaccadeTrial}, min_time=Inf)
    aa = istrialtype(trial, ResponseTrial)
    if aa == false
        return false
    end
    #TODO: Find a way to get the grid so that we can find the aborting saccade
end

"""
    istrialtype(trial::Stimulus.NewEyelinkTRial, ::Type{DistractedTrial})

Return true if `trial` is a distracted trial.

A distracted trial is defined as a trial in which the subject either made a response saccade to the distractor, or to a no-longer valid target.
"""
function istrialtype(trial::Stimulus.NewEyelinkTrial, ::Type{T}, settings::ExperimentSettings) where T <: TrialsAnalysis.AbstractIncorrectOnStimTrial
    aa = istrialtype(trial, IncorrectTrial)
    if aa == false
        return aa
    end
    if length(trial.stimulus) == 0
        return false
    end
    #get the saccaede that breaks the fixation
    saccade = get_aborting_saccade(trial,PreviousTime(-Inf))
    if saccade == nothing
        return false
    end
    endpos = fill(zero(typeof(saccade.end_x)),2,1)
    endpos[:,1] .= [saccade.end_x;saccade.end_y]
    locidx = toidx(endpos, settings)
    if T <: IncorrectOnStimTrial
        return locidx[1] > 0
    end
    if length(trial.stimulus)==2
        stimidx = 2
    else
        stimidx = 1
    end
    if (trial.stimulus[stimidx].id == 2) && (T <: DistractedTrial)
        # distractor
        return locidx[1] == trial.stimulus[stimidx].locidx
    end
    if (trial.stimulus[stimidx].id == 1) && (T <: ConfusedTrial)
        # target set
        return locidx[1] == trial.stimulus[1].locidx
    end
    return false
end

"""
Retrieve all fixation locations from the specified edffile
"""
function get_fixations(edffile::String)

end

"""
Returns all saccades contained in `eyelinkfile`, aligning them to the nearest trial start
"""
function get_all_saccades(eyelinkfile::String)
    eyelinkdata = Eyelink.load(eyelinkfile,1,true,false)
    get_all_saccades(eyelinkdata)
end

function get_all_saccades(eyelinkdata::Eyelink.EyelinkData)
    saccade_events = filter(ee->ee.eventtype == :endsacc, eyelinkdata.events)
    trial_start_events = filter(ee->replace(ee.message, " " => "") == Stimulus.trialevents[:trial_start], eyelinkdata.events)
    get_all_saccades(saccade_events, trial_start_events)
end

function get_all_saccades(saccade_events, trial_start_events)
    trial_start = [ee.sttime for ee in trial_start_events]
    saccade_start = [ee.sttime for ee in saccade_events]
    saccade_angle = [atan(ee.geny - ee.gsty,ee.genx-ee.gstx) for ee in saccade_events]
    aligned_saccade_start = fill(NaN, length(saccade_start))
    trial_idx = fill(0, length(saccade_start))
    #align each saccade to the precedding trial start
    for (ii,ss) in enumerate(saccade_start)
        jj = searchsortedlast(trial_start, ss)
        if 0 < jj <= length(trial_start)
            aligned_saccade_start[ii] = ss - trial_start[jj]
            trial_idx[ii] = jj
        end
    end
    idx = findall(isfinite, aligned_saccade_start)
    aligned_saccade_start[idx], saccade_angle[idx], trial_idx[idx], idx
end

"""
Returns the direction of all correct response saccades for the current session
"""
function get_correct_saccade_angle()
    #TODO: There is something weird with positions of these saccades
    dd2 = MAT.matread("saccade_results.mat")
    trials = TrialsAnalysis.OldTrials(TrialsAnalysis.OldTrialsArgs())
    rtrials = trials[dd2["response_saccades"]["trialindex"]]
    ctrials = Stimulus.getTrialType(rtrials, :reward)
    sidx = findall(in(ctrials), rtrials)
    endx = dd2["response_saccades"]["end_x"][sidx]
    endy = dd2["response_saccades"]["end_y"][sidx]
    startx = dd2["response_saccades"]["start_x"][sidx]
    starty = dd2["response_saccades"]["start_y"][sidx]
    atan.(endy-starty, endx-startx)
end

function TrialsAnalysis.get_trials(::Type{CorrectSaccadeTrial})
    sargs_c = BehaviouralAnalysis.SaccadesArgs(BehaviouralAnalysis.PlexonTimeReference(), BehaviouralAnalysis.CorrectSaccade())
    strials,sidx = cd(DPHT.process_level("session")) do
        saccades = Saccades(sargs_c)
        trials = TrialsAnalysis.get_trials(TrialsAnalysis.OldTrialsArgs())
        trials[saccades.trialidx], saccades.trialidx
    end
    strials, sidx
end

function match_trials(eyelinktrials::Vector{Stimulus.EyelinkTrial}, plexontrials::Vector{Stimulus.Trial})
    fields = fieldnames(Stimulus.Trial)
    words_eye = [to_words(etrial, fields) for etrial in eyelinktrials]
    string_eye = join(map(x->join(x, "->"), words_eye), "=>")
    words_pl = to_words.(plexontrials)
    string_pl = join(map(x->join(x, "->"), words_pl), "=>")
    start_idx = 0
    if length(eyelinktrials) > length(plexontrials)
        idx = findfirst(string_pl, string_eye)
        _words = words_eye
        _string = string_eye

    elseif length(eyelinktrials) < length(plexontrials)
        idx = findfirst(string_eye, string_pl)
        _words = string_pl
        _string = string_pl
    else
        idx = 1:length(string_pl)
        _words = words_pl
        _string = string_pl
    end
    ii = 1
    trialidx = 1
    while ii < first(idx)
        if _string[ii] == '='
            trialidx += 1
        end
        ii += 1
    end
    return trialidx
end

"""
Convert the trial into a sequence of events
"""
function to_words(trial::T,fields=fieldnames(T)) where T <: Stimulus.AbstractTrial
    words = String[]
    ts = Float64[]
    for f in fields
        if f == :target || f == :distractors
            if f == :target
                target = getfield(trial, f)
                if !isnan(target.timestamp)
                    w = "target"
                else
                    w = ""
                end
            else
                target = getfield(trial, f)
                if !isempty(target) && !isnan(target[1].timestamp)
                    w = "distractor"
                    target = target[1]
                else
                    w = ""
                end
            end
            if !isempty(w)
                row,col = (target.row, target.column)
                w = "$(w)($(row),$(col))"
                push!(words, w)
                push!(ts, target.timestamp)
            end
        else
            if f == :start
                _ts = 0.0
            else
                _ts = getfield(trial, f)
            end
            if !isnan(_ts)
                push!(words, string(f))
                push!(ts, _ts)
            end
        end
    end
    sidx = sortperm(ts)
    return words[sidx]
end

function deg2pixels(xy, distance::Real, resolution::Tuple{Int64, Int64}, diagonal::Real) where T <: Real
    ϕ = resolution[1]/resolution[2]
    height = sqrt(diagonal^2/(1+ϕ^2))
    width = ϕ*height
    deg2pixels(xy, distance, resolution, (width,height))
end

"""
	deg2pixels(xy::Union{Tuple{T, T},AbstractVector{T}}, distance::Real, resolution::Tuple{Int64, Int64}, widths::Tuple{Float64, Float64}) where T <: Real

Convert `xy` from degrees to pixels for a monitor that is `distance` centimeters from the eye, having the supplied resolution and pphysical width and height given by `widths`.

# Examples
```jldoctest
julia> deg2pixels([1.0, 1.0], 57.0, (1920,1200), 24*2.5)
2-element Array{Float64,1}:
 37.54111937019342
 37.54111937019342
 ```
"""
function deg2pixels(xy::Union{Tuple{T, T},AbstractVector{T}}, distance::Real, resolution::Tuple{Int64, Int64}, widths::Tuple{Float64, Float64}) where T <: Real
    alength = distance.*(deg2rad.(xy))
    T.(alength.*resolution./widths)
end

function deg2pixels(x::T, args...) where T <: Real
    first(deg2pixels((x,x), args...))
end

function deg2pixels(xy::Union{Tuple{T,T}, AbstractVector{T}}, settings::ExperimentSettings) where T <: Real
    _settings = settings.settings
    deg2pixels(xy,_settings["screen_distance"], round.(Int64, (_settings["screen_width"], _settings["screen_height"])), _settings["screen_size"])
end

"""
	toidx(pos::Matrix{T}, settings::Dict{String,Any};flipy=true) where T <: Real

Convert the pixel positions in `pos` to a location index, based on the information in `settings`.

If `flipy` is `true`, the y-position is flipped. This is the default setting, as `pos` is assumed to represent eyelink gaze coordinates.
"""
function toidx(pos::Matrix{T}, settings::ExperimentSettings;flipy=true) where T <: Real
	#convert target locations from dva to pixels
	pixel_center = (div(settings["screen_width"],2), div(settings["screen_height"],2))
	n_locations = length(settings["target_locations"])
	screen_distance = settings["screen_distance"]
	screen_resolution = (round(Int64,settings["screen_width"]), round(Int64, settings["screen_height"]))
	screen_size = 2.5*settings["screen_size"]
	target_locations = [pixel_center .+ deg2pixels(Float32.(settings["target_locations"][i]),
			screen_distance, screen_resolution, screen_size) for i in 1:n_locations]
	w2,h2 = deg2pixels((settings["target_window_size"],settings["target_window_size"]),
		screen_distance, screen_resolution, screen_size)

	if flipy
		# correct for the fact that eyelink has the origin at the top of the screen
		endpos = [pos[1:1,:];screen_resolution[2] .- pos[2:2,:]]
	else
		endpos = pos
	end
	target_rects = [GeometryTypes.SimpleRectangle((target_locations[ii] - [w2/2, w2/2])..., w2, w2) for ii in 1:n_locations]
	locidx = fill(0, size(pos,2))
	for i in 1:length(locidx)
		for k in 1:length(target_rects)
			if GeometryTypes.isinside(target_rects[k], endpos[:,i]...)
				locidx[i] = k
				break
			end
		end
	end
	locidx
end
