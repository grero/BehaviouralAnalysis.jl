module BehaviouralAnalysis
using Stimulus
using Eyelink
using MAT
using BSON
using JSON
using Glob
using FileIO
using FixedPointNumbers
using DataProcessingHierarchyTools
using DataProcessingHierarchyTools: StableHashes
using TrialsAnalysis
using GeometryTypes
const DPHT = DataProcessingHierarchyTools

include("types.jl")
include("tools.jl")
include("eye.jl")
include("inactive_trials.jl")
include("allsaccades.jl")
include("aborted_saccades.jl")

export CueAligned, SaccadeAligned, TargetAligned, DistractorAligned, LeftFixationAligned, CorrectSaccadeTrial, AbortedSaccadeTrial, NoSaccadeTrial, CorrectTrial, IncorrectTrial

"""
Get trials of type `trialtype` where a respsonse was given
"""
function get_response_trials(trialtype::Symbol)
    dir = DPHT.process_level("day")
    if isfile(joinpath(dir,"event_markers.csv"))
        trials = TrialsAnalysis.get_trials(Stimulus.NewTrial)
        #figure out which session we came from
        session_name = DPHT.get_level_name("session")
        if isempty(session_name)
            cidx = findall(t->t.reward_on > 0.0, trials)
            ctrials = trials[cidx]
        else
            session_starts = cd(dir) do
                 TrialsAnalysis.get_session_starts()
            end
            session_nr = parse(Int64, DPHT.get_numbers(session_name))
            cidx = findall(t->(t.reward_on > 0.0)&(t.sessionid==session_nr), trials.trials)
            ctrials = trials[cidx]
            for trial in ctrials
                trial.trial_start -= session_starts[session_nr]
            end
        end
    else
        dir = DPHT.process_level("session")
        cd(dir) do
            dd2 = MAT.matread("saccade_results.mat")
            #FIXME: Make this more general
            trials = TrialsAnalysis.OldTrials(TrialsAnalysis.OldTrialsArgs())
            rtrials = filter(t->isfinite(t.response), trials.trials)[dd2["response_trial_idx"]]
            ctrials = filter(t->isfinite(getfield(t, trialtype)), rtrials)
            trial_labels = Stimulus.getTrialLocationLabel(ctrials, :target)
            cidx = findall(in(rtrials), ctrials)
            ctrials, cidx
        end
    end
    ctrials, cidx
end

function get_correct_response_trials(session)
    session_dir = Spiketrains.expand_session(session)
    session_dir = joinpath(session_dir, "session01")
    cd(session_dir) do
        get_correct_response_trials()
    end
end


get_correct_response_trials() = get_response_trials(:reward)

function get_aborted_trials()
    dir = DPHT.process_level("session")
    cd(dir) do
        dd = MAT.matread("saccade_results.mat")
        trials = TrialsAnalysis.OldTrials(TrialsAnalysis.OldTrialsArgs())
        aborted_time = dd["aborted_saccades"]["start_time"]/1000.0
        cidx = dd["aborted_saccades"]["trialindex"]
        vidx = findall(t->(t.failure>0)&&(t.target.timestamp>0)&&(t.failure > t.target.timestamp), trials[cidx])
        trials[cidx[vidx]], vidx
    end
end

function get_reaction_time(session)::Vector{Float64}
    #rdir = DPHT.process_level("days")
    rdir = "."
    session_dir = Spiketrains.expand_session(session)
    session_dir = joinpath(rdir, session_dir, "session01")
    cd(session_dir) do
        get_reaction_time()
    end
end

"""
Get reaction time in ms for the correct trials.
"""
function get_reaction_time(trialtype::Symbol=:correctsaccadetrial)::Union{Nothing, Vector{Float64}}
    saccade_type = get_saccade_type(trialtype)
    if saccade_type == nothing
        return nothing
    end
    sargs_c = SaccadesArgs(PlexonTimeReference(),saccade_type())
    saccades = BehaviouralAnalysis.Saccades(sargs_c)
    trials = TrialsAnalysis.get_trials()
    rtime = saccades.onset - 1000*[TrialsAnalysis.get_trial_start(trials[ii]) + TrialsAnalysis.get_response_start(trials[ii]) for ii in saccades.trialidx]
    rtime
end

"""
Get spike triggered location
"""
function get_location(timestamps::AbstractVector{Float64}, gx::AbstractVector{T}, gy::AbstractVector{T};window=-9:10,screen_width=1200, screen_height=1920, sampling_rate=30_000) where T <: Real
    M = fill(0.0, screen_height, screen_width)
    for tt in timestamps
        idx = (round(Int64, tt)) .+ window
        if last(idx) <= length(gx)
            col = rount(Int64, mean(gx[idx]))
            row = rount(Int64, mean(gy[idx]))
        else
            break
        end
        if 0 < row < screen_height && 0 < col < screen_width
            M[row,col] += 1.0
        end
    end
    M
end

end # module
