import TrialsAnalysis: AbstractTrialType, CorrectTrial, IncorrectTrial, ResponseTrial, AbortedTrial, DistractedTrial, ConfusedTrial, IncorrectOnStimTrial, CorrectBeforeCorrect, CorrectAfterCorrect, CorrectDistractorTrial, JumpTheGunTrial
import StableHashes: shash
struct CorrectSaccadeTrial <: AbstractTrialType end
struct AbortedSaccadeTrial <: AbstractTrialType end

struct NoSaccadeTrial <: AbstractTrialType
    tmin::Float64
end

trialtype_reverse_map = Dict(:correctsaccadetrial => CorrectSaccadeTrial,
                             :abortedsaccadetrial => AbortedSaccadeTrial,
                             :nosaccadetrial => NoSaccadeTrial,
                             :correcttrial => CorrectTrial,
							 :incorrecttrial => IncorrectTrial,
                             :distractedtrial => DistractedTrial,
                             :confusedtrial => ConfusedTrial,
                             :incorrectonstimtrial => IncorrectOnStimTrial,
                             :correctbeforecorrect => CorrectBeforeCorrect,
                             :correctaftercorrect => CorrectAfterCorrect,
                             :correctdistractortrial => CorrectDistractorTrial,
							 :jumptheguntrial => JumpTheGunTrial)

function get_trialtype(::Type{T}) where T <: AbstractTrialType
    if T <: CorrectSaccadeTrial
        return :correctsaccadetrial
    elseif T <: AbortedSaccadeTrial
        return :abortedsaccadetrial
    elseif T <: NoSaccadeTrial
        return :nosaccadetrial
    elseif T <: CorrectTrial
        return :correcttrial
    elseif T <: DistractedTrial
        return :distractedtrial
    elseif T <: ConfusedTrial
        return :confusedtrial
    elseif T <: IncorrectOnStimTrial
        return :incorrectonstimtrial
    elseif T <: CorrectBeforeCorrect
        return :correctbeforecorrect
    elseif T <: CorrectAfterCorrect
        return :correctaftercorrect
    elseif T <: CorrectDistractorTrial
        return :correctdistractortrial
	elseif T <: JumpTheGunTrial
		return :jumptheguntrial
    end
    return :unknown
end

function shash(::Type{AbortedSaccadeTrial}, h::UInt64)
    shash(:AbortedSaccadeTrial, h)
end

function shash(::Type{CorrectSaccadeTrial}, h::UInt64)
	objid = 0xe2f80a564d8872f7
	StablesHshes.shash_uint(3h - objid)
end
function shash(::Type{CorrectTrial}, h::UInt64)
    shash("CorrectTrial", h)
end

function shash(::Type{DistractedTrial}, h::UInt64)
	objid = 0xdae2495ea8175260
	StableHashes.shash_uint(3h - objid)
end

function shash(::Type{ConfusedTrial}, h::UInt64)
	objid = 0xbcf4c1958956f180
	StableHashes.shash_uint(3h - objid)
end

function shash(::Type{IncorrectOnStimTrial}, h::UInt64)
	objid = 0xbb0e5f6a8981f332
	StableHashes.shash_uint(3h - objid)
end

function shash(::Type{CorrectBeforeCorrect}, h::UInt64)
	objid = 0x7a5fe57e5d8acdcb
	StableHashes.shash_uint(3h - objid)
end

function shash(::Type{CorrectAfterCorrect}, h::UInt64)
	objid = 0xa206efba584547ec
	StableHashes.shash_uint(3h - objid)
end

function shash(::Type{CorrectDistractorTrial}, h::UInt64)
	objid = 0x692ac78a2ee234ec
	StableHashes.shash_uint(3h - objid)
end

function shash(::Type{JumpTheGunTrial}, h::UInt64)
	objid = 0xf9dbe5f9373e76a6
	StableHashes.shash_uint(3h - objid)
end

function get_ntrials(::Type{AbortedSaccadeTrial})
    _ntrials = cd(DPHT.process_level("session")) do
        dd = MAT.matread("saccade_results.mat")
        length(dd["aborted_saccades"]["trialindex"])
    end
    _ntrials
end

abstract type AbstractExperimentSettingsArgs <: DPHT.DPHDataArgs end

struct ExperimentSettingsArgs <: AbstractExperimentSettingsArgs end
struct OldExperimentSettingsArgs <: AbstractExperimentSettingsArgs end

struct ExperimentSettings{T <: AbstractExperimentSettingsArgs} <: DPHT.DPHData
	settings::Dict{String,Any}
	args::T
end

Base.getindex(X::ExperimentSettings{T},args...) where T <: AbstractExperimentSettingsArgs = getindex(X.settings,args...)
Base.getkey(X::ExperimentSettings{T},args...) where T <: AbstractExperimentSettingsArgs = getkey(X.settings, args...)

"""
	ExperimentSettings(args::ExperimentSettingsArgs)

Load the settings used during the experiment for the current session.
"""
function ExperimentSettings(args::ExperimentSettingsArgs)
	_date = DPHT.get_level_name("day")
	_sn = filter(isdigit,DPHT.get_level_name("session"))
	_aa = lowercase(DPHT.get_level_name("subject"))
	# process James and pancake separately, since the values here are just hard coded
	fname = "$(_aa[1])$(_date[5:end])$(_sn)_settings.txt"
	if !islink(fname) && !isfile(fname)
		fname = "$(uppercase(_aa[1]))$(_date[5:end])$(_sn)_settings.txt"
	end
	if !islink(fname) && !isfile(fname)
		# old format
		mth = parse(Int64, _date[5:6])
		dd = parse(Int64, _date[7:8])
		snn = parse(Int64, _sn)
		fname = "$(_aa[1])$(mth)_$(dd)_$(snn)_settings.txt"
	end
	if islink(fname) && DPHT.git_annex != nothing
		run(`$(DPHT.git_annex()) get $fname`)
	end
	settings = JSON.parsefile(fname)
	ExperimentSettings(settings, args)
end

function ExperimentSettings(args::OldExperimentSettingsArgs)
	_aa = DPHT.get_level_name("subject")
	if _aa in ["Pancake","James"]
		if _aa == "Pancake"
			_grid = Stimulus.Grid(1440,900, 5,5)
		else
			_grid = Stimulus.Grid(1920,1200, 5,5)
		end
		settings = Dict{String,Any}(string(k) => getfield(_grid, k) for k in fieldnames(Stimulus.Grid))
	else
		error("Unkown subject $(_aa)")
	end
	ExperimentSettings(settings, args)
end

function get_targets(settings::ExperimentSettings{OldExperimentSettingsArgs})
	xdiff = settings["xdiff"]
	xmargin = settings["xmargin"]
	ydiff = settings["ydiff"]
	ymargin = settings["ymargin"]
	screen_height = settings["screen_height"]
	target_rects = GeometryTypes.SimpleRectangle[]
	for col in 1:settings["columns"]
		x = xmargin + (col-1)*xdiff
		for row in 1:settings["rows"]
			y = screen_height - (ymargin + row*ydiff)
			push!(target_rects, GeometryTypes.SimpleRectangle(x,y, xdiff, ydiff))
		end
	end
	target_rects
end
