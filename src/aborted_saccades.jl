struct AbortedSaccadesArgs <: DPHT.DPHDataArgs end

struct AbortedSaccades <: DPHT.DPHData
    trialidx::Vector{Int64}
    onset::Vector{Float64}
    offset::Vector{Float64}
    startpos::Matrix{Float64}
    endpos::Matrix{Float64}
    args::AbortedSaccadesArgs
end

DPHT.level(::Type{AbortedSaccades}) = "session"
DPHT.filename(::Type{AbortedSaccades}) = "aborted_saccades.mat"
DPHT.datatype(::Type{AbortedSaccadesArgs}) = AbortedSaccades


function AbortedSaccades(args::AbortedSaccadesArgs;do_save=true, force_redo=false)
    fname = DPHT.filename(args)
    redo = force_redo || !isfile(fname)
    if !redo
        X = DPHT.load(args)
    else
        ff = "eyetrials.mat"
        if HDF5.ishdf5(ff)
            dd = HDF5.h5open(ff)
            ntrials = size(dd["et/data/trials/start"],2)
            _failure = read(dd["et/data/trials/failure"])
            _response = read(dd["et/data/trials/response_cue"])
            _trial_start = read(dd["et/data/trials/start"])
            failure = fill(NaN, ntrials)
            response = fill(NaN, ntrials)
            trialstart = fill(NaN, ntrials)
            for j in 1:ntrials
                rr = read(dd[_response[j]])
                if !(any(isnan.(rr)) || all(rr.==zero(eltype(rr))))
                    response[j] = rr[1,1]
                end
                ff = read(dd[_failure[j]])
                if !(any(isnan.(ff)) || all(ff.==zero(eltype(ff))))
                    failure[j]  = ff[1,1]
                end
            end
            trial_start = cat([read(dd[_trial_start[1,i]]) for i in 1:ntrials]...,dims=1)[:]
            _grid = Stimulus.Grid(read(dd["et/data/screen_size"])..., 5,5)
            #the saccades for each trial are stored as reference objects
            saccades = Vector{Dict{String,Any}}(undef, length(trial_start))
            for i in 1:length(trial_start)
                _saccade = read(dd[dd["et/data/trials/saccade/"][1,i][1,1]])
                saccades[i] = Dict{String,Any}()
                if all(_saccade .== zero(UInt64))
                    continue  #no valid saccade for this trial
                end
                for (k,v) in _saccade
                    if !isa(v[1,1], HDF5.HDF5ReferenceObj)
                        saccades[i][k] = v
                        continue
                    end
                    vv = read(dd[v[1,1]])
                    saccades[i][k] = Vector{eltype(vv)}(undef, size(v,2))
                    saccades[i][k][1] = vv[1,1]
                    for j in 2:size(v,2)
                        vv = read(dd[v[1,j]])
                        saccades[i][k][j] = vv[1,1]
                    end
                end
            end
        else
            Q = MAT.matread(ff)
            failure = Q["et"]["data"]["trials"]["failure"][:]
            response = Q["et"]["data"]["trials"]["response_cue"][:]
            trial_start = Q["et"]["data"]["trials"]["start"][:]
            saccades = Q["et"]["data"]["trials"]["saccade"][:]
            _grid = Stimulus.Grid(Q["et"]["data"]["screen_size"]..., 5,5)
            ntrials = length(trial_start)
        end
        tidx = Int64[]
        for ii in 1:ntrials
            #skip trials without a failure signal, i.e. where the subject did nothing
            if isempty(failure[ii]) || any((!isfinite).(failure[ii]))
                continue
            end
            if isempty(response[ii]) || any((!isfinite).(response[ii]))
                push!(tidx,ii)
            end
        end
        naborted = length(tidx)
        onset = fill(0.0, naborted)
        offset = fill(0.0, naborted)
        startpos = fill(0.0, 2, naborted)
        endpos = fill(0.0, 2, naborted)
        skip_trials = Int64[]
        _grid_center = (div(_grid.screen_width,2),div(_grid.screen_height,2))
        aa = DPHT.get_level_name("subject")
        if aa == "Pancake"
            fixsize = div(100,2)
        elseif aa == "James"
            fixsize = div(200,2)
        else
            fixsize = div(150,2)  # fixation area was always kept as 150 by 150 px window centered at the center of the screen
        end

        for (ii,_tidx) in enumerate(tidx)
            _saccades = saccades[_tidx]
            if isempty(_saccades)
                push!(skip_trials, ii)
                continue
            end
            #the aborting saccade should be the first saccade that goes from inside the fixation window to outside
            sidx = 1
            found = false
            while (sidx <= length(_saccades["onset"])) && !found
                startx = _saccades["startx"][sidx]
                starty = _saccades["starty"][sidx]
                endx = _saccades["endx"][sidx]
                endy = _saccades["endy"][sidx]
                start_inside = _grid_center[1] - fixsize < startx < _grid_center[1] + fixsize
                start_inside &= _grid_center[2] - fixsize < starty < _grid_center[2] + fixsize
                end_outside = endx  < _grid_center[1] - fixsize || endx > _grid_center[1] + fixsize
                end_outside |= endy  < _grid_center[2] - fixsize || endy > _grid_center[2] + fixsize

                if start_inside && end_outside
                    found = true
                else
                    sidx +=1
                end
            end
            if !found
                continue
            end
            onset[ii] = float(_saccades["onset"][sidx]) - float(trial_start[_tidx])
            offset[ii] = float(_saccades["offset"][sidx]) - float(trial_start[_tidx])
            startpos[:,ii] = [_saccades["startx"][sidx], _saccades["starty"][sidx]]
            endpos[:,ii] = [_saccades["endx"][sidx], _saccades["endy"][sidx]]
        end
        X = AbortedSaccades(setdiff(tidx,skip_trials), onset, offset, startpos, endpos, args)
        if do_save
            DPHT.save(X)
        end
    end
    X
end

"""
Get the aborting saccades for the current session
"""
function get_aborting_saccade(trialtype::Type{TT}=TrialsAnalysis.AbortedTrial) where TT <: AbstractTrialType
    aa = DPHT.get_level_name("subject")
    if aa == "Pancake"
        fixsize = div(100,2)
        _grid = Stimulus.Grid(1440,900, 5,5)
		fixcenter = (div(_grid.screen_width,2), div(_grid.screen_height,2))
		T = Stimulus.EyelinkTrial
		args = [fixcenter,fixsize]
    elseif aa == "James"
        fixsize = div(200,2)
        _grid = Stimulus.Grid(1920,1200, 5,5)
		fixcenter = (div(_grid.screen_width,2),div(_grid.screen_height,2))
		T = Stimulus.EyelinkTrial
		args = [fixcenter,fixsize]
    else
		settings_files = glob("*_settings.txt")
		if isempty(settings_files)
			error("No settings file found")
		end
        settings = ExperimentSettings(ExperimentSettingsArgs())
        fixsize = deg2pixels(settings.settings["fixation_window"], settings)
        fixcenter = (div(settings.settings["screen_width"],2),div(settings.settings["screen_height"],2))
		#TODO: We need to convert this from dva to pixels
		#we don't have a grid here, but we can get the fixation window from the settings
		T = Stimulus.NewEyelinkTrial
		args = ()
    end
    eyetrials = get_eyelinktrials(EyelinkTrialsArgs(T))
    get_aborting_saccade(eyetrials.trials, args...;trialtype=trialtype)
end

function get_aborting_saccade(etrial::Stimulus.EyelinkTrial, grid::Stimulus.Grid, fixsize;kvs...)
    get_aborting_saccade(etrial.saccades, grid, fixsize;kvs...)
end

function get_aborting_saccade(etrial::Stimulus.NewEyelinkTrial,tmin::PreviousTime)
	saccade_onset = [saccade.start_time for saccade in etrial.saccades]
	sidx = searchsortedlast(saccade_onset, etrial.left_fixation)
	if sidx == 0
		return nothing
	end
	saccade = etrial.saccades[sidx]
	if saccade.start_time > tmin.time
		return saccade
	else
		return nothing
	end
end

function get_aborting_saccade(etrial::Stimulus.NewEyelinkTrial,tmax::NextTime)
	saccade_onset = [saccade.start_time for saccade in etrial.saccades]
	sidx = searchsortedlast(saccade_onset, etrial.left_fixation)
	if sidx == 0
		return nothing
	end
	saccade = etrial.saccades[sidx]
    if saccade.start_time < tmax.time
		return saccade
	else
		return nothing
	end
end

get_aborting_saccade(etrial::Union{Stimulus.EyelinkTrial,Stimulus.NewEyelinkTrial}, args...;kvs...) = get_aborting_saccade(etrial.saccades, args...;kvs...)

function get_aborting_saccade(saccades::Vector{Eyelink.AlignedSaccade}, grid::Stimulus.Grid, fixsize,args...)
    _grid_center = (div(grid.screen_width,2),div(grid.screen_height,2))
	get_aborting_saccade(saccades, _grid_center, fixsize,args...)
end

inside_to_outside(saccade, fixcenter,fixsize) = inside_to_outside(saccade.start_x, saccade.start_y, saccade.end_x, saccade.end_y, fixcenter, fixsize)

function inside_to_outside(start_x,start_y,end_x, end_y, fixcenter::Tuple{T,T}, fixsize) where T <: Real
    start_inside = fixcenter[1] - fixsize < start_x < fixcenter[1] + fixsize
    start_inside &= fixcenter[2] - fixsize < start_y < fixcenter[2] + fixsize
    end_outside = end_x  < fixcenter[1] - fixsize || end_x > fixcenter[1] + fixsize
    end_outside |= end_y  < fixcenter[2] - fixsize || end_y > fixcenter[2] + fixsize
    start_inside && end_outside
end

function get_aborting_saccade(saccades::Vector{Eyelink.AlignedSaccade}, fixcenter::Tuple{T,T}, fixsize,tmin::PreviousTime=PreviousTime(-Inf)) where T <: Real
    sidx = 0
    for (ii,s) in enumerate(saccades)

        # find the first saccade starting from inside fixation going outside the fixation after tmin
        bb = inside_to_outside(s.start_x, s.start_y, s.end_x, s.end_y, fixcenter, fixsize)
        if bb  && s.start_time >= tmin.time
            sidx = ii
            break
        end
    end
    if sidx == 0
        return nothing
    end
    return saccades[sidx]
end

function get_aborting_saccade(saccades::Vector{Eyelink.AlignedSaccade}, fixcenter::Tuple{T,T}, fixsize,tmax::NextTime) where T <: Real
    sidx = 0
    for (ii,s) in enumerate(reverse(saccades))
        bb = inside_to_outside(s.start_x, s.start_y, s.end_x, s.end_y, fixcenter, fixsize)
        if bb && s.start_time <= tmax.time
            sidx = ii
            break
        end
    end
    if sidx == 0
        return nothing
    end
    return saccades[end-sidx+1]
end

function get_aborting_saccade(etrials::Vector{T}, args...;trialtype=TrialsAnalysis.AbortedTrial) where T <: Stimulus.AbstractEyeTrial
    tidx = Int64[]
    onset = Float64[]
    offset = Float64[]
    startpos = Vector{Float64}[]
    endpos = Vector{Float64}[]

    for (ii,trial) in enumerate(etrials)
        #TODO: This condition is the only thing that separates saccades that abort the trial from correct saccades
        if !TrialsAnalysis.istrialtype(trial, trialtype)
            continue
        end
        if trialtype == TrialsAnalysis.CorrectTrial
			if T <: Stimulus.EyelinkTrial
                tmin = PreviousTime(trial.response)
			else
                tmin = PreviousTime(trial.response_on)
			end
        elseif trialtype == TrialsAnalysis.AbortedTrial
            tmin = NextTime(trial.failure)
        elseif trialtype == TrialsAnalysis.JumpTheGunTrial
            tmin = NextTime(trial.failure)
        else
            tmin = PreviousTime(-Inf)
        end
        asaccade = get_aborting_saccade(trial, args...,tmin)
        if asaccade != nothing
            push!(endpos, [asaccade.end_x, asaccade.end_y])
            push!(startpos, [asaccade.start_x, asaccade.start_y])
            push!(offset, asaccade.end_time)
            push!(onset, asaccade.start_time)
            push!(tidx, ii)
        end
    end
    tidx, onset, offset, startpos, endpos
end
