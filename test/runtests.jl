using BehaviouralAnalysis
using Eyelink
using Stimulus
using TrialsAnalysis
using BSON
using DataProcessingHierarchyTools
using DataProcessingHierarchyTools: shash
const DPHT = DataProcessingHierarchyTools
using StatsBase
using Downloads
using Test

@testset "Alignments" begin
	@test BehaviouralAnalysis.alignment_map[:fixation] == BehaviouralAnalysis.FixationAligned()
	@test BehaviouralAnalysis.get_alignment(Stimulus.NewTrial[], BehaviouralAnalysis.FixationAligned()) == :fix_start

	@test BehaviouralAnalysis.alignment_map[:stimulus1] == BehaviouralAnalysis.StimulusAligned(1)
	@test BehaviouralAnalysis.shortname(BehaviouralAnalysis.StimulusAligned(1)) == "stimulus1"
	@test BehaviouralAnalysis.get_alignment(Stimulus.NewTrial[], BehaviouralAnalysis.StimulusAligned(1)) == :stimulus1
end

@testset "Type maps" begin
    @test BehaviouralAnalysis.trialtype_reverse_map[:distractedtrial] == BehaviouralAnalysis.DistractedTrial
    @test BehaviouralAnalysis.get_trialtype(BehaviouralAnalysis.DistractedTrial) == :distractedtrial
    @test BehaviouralAnalysis.trialtype_reverse_map[:confusedtrial] == BehaviouralAnalysis.ConfusedTrial
    @test BehaviouralAnalysis.get_trialtype(BehaviouralAnalysis.ConfusedTrial) == :confusedtrial
    @test BehaviouralAnalysis.trialtype_reverse_map[:incorrectonstimtrial] == BehaviouralAnalysis.IncorrectOnStimTrial
    @test BehaviouralAnalysis.get_trialtype(BehaviouralAnalysis.IncorrectOnStimTrial) == :incorrectonstimtrial
    @test shash(BehaviouralAnalysis.DistractedTrial) == 0xb645e32d4548ce9d
    @test shash(BehaviouralAnalysis.ConfusedTrial) == 0xdfbb6ce000f1bc73
    @test shash(BehaviouralAnalysis.IncorrectOnStimTrial) == 0xdc7825afe5eb4a02
    @test BehaviouralAnalysis.trialtype_reverse_map[:correctbeforecorrect] == BehaviouralAnalysis.CorrectBeforeCorrect
    @test BehaviouralAnalysis.get_trialtype(BehaviouralAnalysis.CorrectBeforeCorrect) == :correctbeforecorrect
    @test BehaviouralAnalysis.trialtype_reverse_map[:correctaftercorrect] == BehaviouralAnalysis.CorrectAfterCorrect
    @test BehaviouralAnalysis.get_trialtype(BehaviouralAnalysis.CorrectAfterCorrect) == :correctaftercorrect
    @test BehaviouralAnalysis.get_trialtype(BehaviouralAnalysis.CorrectDistractorTrial) == :correctdistractortrial
    @test shash(BehaviouralAnalysis.CorrectDistractorTrial) == 0xad6ca432444f94bf
    @test BehaviouralAnalysis.trialtype_reverse_map[:jumptheguntrial] == BehaviouralAnalysis.JumpTheGunTrial
    @test BehaviouralAnalysis.get_trialtype(BehaviouralAnalysis.JumpTheGunTrial) == :jumptheguntrial
    @test shash(BehaviouralAnalysis.JumpTheGunTrial) == 0x567d2c5dcc39f3bf
end

@testset "Tools" begin
    trial_events = Eyelink.Event[Eyelink.Event(0x00000000, 0x0009be53, 0x00000000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0, "0  0  0  0  0  0  0  0", :messageevent), Eyelink.Event(0x00000000, 0x000a423a, 0x00000000, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0, "0  0  0  0  0  0  0  0", :messageevent)]
    nt = BehaviouralAnalysis.get_ntrials(trial_events)
    @test nt == 2
end

@testset "Hash" begin
    all_saccade_args = BehaviouralAnalysis.SaccadesArgs(BehaviouralAnalysis.PlexonTimeReference(), BehaviouralAnalysis.SpontaneousSaccade())
    fname = DPHT.filename(all_saccade_args)
    @test fname == "spontaneous_saccades_2143c876c044da5e.mat"
end
@testset "Data" begin
    data_dir = "/Users/roger/Documents/research/monkey/newWorkingMemory/James/20140904/session01/"
    if isdir(data_dir)
        cd(data_dir) do
            ctrials,cidx = BehaviouralAnalysis.get_correct_response_trials()
            @test length(cidx) == 481
            abtrials,aidx = BehaviouralAnalysis.get_aborted_trials()
            @test length(aidx) == 73
        end
    end
end

@testset "NewData" begin
    data_dir = "/Users/roger/Documents/research/monkey/newWorkingMemory/Wiesel/20181109/"
    if !isfile(joinpath(data_dir,"event_markers.csv"))
        tdir = tempdir()
        data_dir = joinpath(tdir, "Wiesel","20181109")
        mkpath(data_dir)
        Downloads.download("http://cortex.nus.edu.sg/testdata/w9_11_event_markers.csv", joinpath(data_dir, "event_markers.csv"))
    end
    if isdir(data_dir)
        ctrials, cidx = cd(data_dir) do
            TrialsAnalysis.get_trials(BehaviouralAnalysis.CorrectTrial)
        end
        n = length(ctrials)
        @test n == 355
        ctrials, cidx = cd(data_dir) do
            TrialsAnalysis.get_trials(BehaviouralAnalysis.CorrectTrial, TrialsAnalysis.Trials)
        end
        n = length(ctrials)
        @test n == 355
        ctrials, cidx = cd(data_dir) do
            TrialsAnalysis.get_trials(BehaviouralAnalysis.CorrectAfterCorrect)
        end
        @test length(cidx) == 88
        ctrials, cidx = cd(data_dir) do
            TrialsAnalysis.get_trials(BehaviouralAnalysis.CorrectBeforeCorrect)
        end
        @test length(cidx) == 88

        @testset "Returning trials based on session" begin
            data_dir = "/Users/roger/Documents/research/monkey/newWorkingMemory/Wiesel/20181126/"
            if !isfile(joinpath(data_dir,"event_markers.csv"))
                tdir = tempdir()
                data_dir = joinpath(tdir, "Wiesel","20181126")
                mkpath(data_dir)
                Downloads.download("http://cortex.nus.edu.sg/testdata/w11_26_event_markers.csv", joinpath(data_dir, "event_markers.csv"))
                mkpath(joinpath(data_dir, "session01"))
                mkpath(joinpath(data_dir, "session02"))
            end
            if isdir(data_dir)
                for (tt, nc, session) in zip([80.7014, 6.442433333332701], [212,113], ["session01", "session02"])
                    _data_dir = joinpath(data_dir, session)
                    ctrials, cidx = cd(_data_dir) do
                        BehaviouralAnalysis.get_correct_response_trials()
                    end
                    n = length(ctrials)
                    @test n == nc
                    st = ctrials[1].trial_start
                    @test st ≈ tt
                end
            end
        end
    end
end

@testset "Eye" begin
    function fake_event(sstime::UInt32, message="";kvs...)
        args = Any[]
        dd = Dict(kvs)
        for f in fieldnames(Eyelink.Event)
            if (f == :time) || (f == :sttime)
                push!(args, sstime)
            elseif f == :entime
                push!(args, get(dd, :entime, sstime))
            elseif f == :message
                push!(args, message)
            elseif f == :eventtype
                push!(args, get(dd, :eventtype, :endsacc))
            elseif f in keys(dd)
                push!(args,dd[f])
            else
                push!(args, zero(fieldtype(Eyelink.Event, f)))
            end
        end
        Eyelink.Event(args...)
    end
    saccade_events = [fake_event(UInt32(101),entime=UInt32(103)),
                      fake_event(UInt32(234);gstx=10.0f0, gsty=10.0f0, genx=3.0f0, geny=3.0f0,entime=UInt32(240)),
                      fake_event(UInt32(1224);gstx=10.0f0, gsty=10.0f0, genx=3.0f0, geny=3.0f0, entime=UInt32(1230))]
    trial_start_events = [fake_event(UInt32(231), "00000000",eventtype=:messageevent), fake_event(UInt32(1221), "00000000", eventtype=:messagevent)]
    aligned_time, angle, tidx, idx = BehaviouralAnalysis.get_all_saccades(saccade_events, trial_start_events)
    @test aligned_time ≈ [3.0, 3.0]
    @test idx == [2,3]
    @test tidx == [1,2]
    @test angle ≈ [-0.75π, -0.75π]
    events = [saccade_events;trial_start_events]
    sort!(events, by=ee->ee.sttime)
    all_saccades = convert(BehaviouralAnalysis.Saccades{BehaviouralAnalysis.EyelinkTimeReference, BehaviouralAnalysis.SpontaneousSaccade}, events)
    @test all_saccades.onset ≈ [3.0, 3.0]
    @test all_saccades.duration ≈ [6.0, 6.0]
    @test all_saccades.trialidx == [1,2]
    @test all_saccades.angle ≈ [-0.75π, -0.75π]

    @testset "Data" begin
        tdir = tempdir()
        cd(tdir) do
            datapath = joinpath("James","20141010","session01")
            mkpath(datapath)
            cd(datapath) do
                Downloads.download("http://cortex.nus.edu.sg/testdata/J10_10.edf", "J10_10.edf")
                Downloads.download("http://cortex.nus.edu.sg/testdata/J20141010_event_data.mat", "event_data.mat")
                eyetrials = BehaviouralAnalysis.get_eyelinktrials(BehaviouralAnalysis.EyelinkTrialsArgs();force_redo=true)
                files = readdir()
                fname = DPHT.filename(eyetrials.args)
                @test fname == "eyetrials.bson"
                @test isfile(fname)
                @test length(eyetrials.trials) == 1081
                eyetrials2 = BehaviouralAnalysis.get_eyelinktrials(BehaviouralAnalysis.EyelinkTrialsArgs(), force_redo=false)
                @test length(eyetrials2.trials) == length(eyetrials.trials)
                trials = TrialsAnalysis.get_trials()

                @testset "Matching trials" begin
                    @test length(trials) == length(eyetrials.trials)
                    tidx = BehaviouralAnalysis.match_trials(eyetrials.trials, trials.trials[100:end])
                    @test tidx == 100
                    tidx = BehaviouralAnalysis.match_trials(eyetrials.trials[100:end], trials.trials)
                    @test tidx == 100
                    tidx = BehaviouralAnalysis.match_trials(eyetrials.trials, trials.trials)
                    @test tidx == 1
                end
            end
            rm(datapath,recursive=true)
        end
    end
end

@testset "Aborting saccades" begin
    saccades = [Eyelink.AlignedSaccade(0.1710000000002765, 0.18499999999949068, 1027.5f0, 586.3f0, 963.1f0, 570.8f0, 1, :start),
 Eyelink.AlignedSaccade(1.1049999999995634, 1.1329999999998108, 962.4f0, 583.8f0, 841.3f0, 732.1f0, 1, :start),
 Eyelink.AlignedSaccade(1.2229999999999563, 1.2510000000002037, 833.8f0, 744.5f0, 966.3f0, 598.5f0, 1, :start)]
    _grid = Stimulus.Grid(1920,1200,5,5)
    fixsize = div(200,2)
    asaccade = BehaviouralAnalysis.get_aborting_saccade(saccades,_grid,fixsize)

    @test asaccade.start_time ≈ 1.1049999999995634
    @test asaccade.start_x ≈ 962.4f0
    @test asaccade.start_y ≈ 583.8f0
    @test asaccade.end_x ≈ 841.3f0
    @test asaccade.end_y ≈ 732.1f0
    asaccade = BehaviouralAnalysis.get_aborting_saccade(saccades[1:1], _grid, fixsize)
    @test asaccade == nothing

    etrial = Stimulus.EyelinkTrial(Stimulus.Target(4,2,0.8679999999994834),
                                   [Stimulus.Target(0.0 ,0.0, NaN)],
                                   4768.965, 4770.313, 0.5500000000001819,
                                   NaN, NaN, NaN, NaN, NaN, 1.2789999999995416,
                                   saccades)
    tidx, onset, offset, startpos, endpos = BehaviouralAnalysis.get_aborting_saccade([etrial], _grid, fixsize)
    @test tidx == [1]
    @test onset ≈ [1.1049999999995634]
    @test offset ≈ [1.1329999999998108]
    @test startpos ≈ [[962.4f0, 583.8f0]]
    @test endpos ≈ [[841.3f0,732.1f0]]

    @testset "Data" begin
        data_dir = joinpath(homedir(), "Documents","research","monkey", "newWorkingMemory","James","20140904","session01")
        if isdir(data_dir)
            cd(data_dir) do
                trialidx, onset, offset, startpos, endpos = BehaviouralAnalysis.get_aborting_saccade()
                shash(trialidx) == 0x8fd05aa50c147a5f
            end
        end
    end
end

@testset "Inactive trials" begin
    tdir = tempdir()
    cd(tdir) do
        data_dir = joinpath("James","20140904","session01")
        mkpath(data_dir)
        Downloads.download("http://cortex.nus.edu.sg/testdata/event_data.mat", joinpath(data_dir, "event_data.mat"))
        Downloads.download("http://cortex.nus.edu.sg/testdata/eyetrials.bson", joinpath(data_dir, "eyetrials.bson"))
        if isdir(data_dir)
            midx = cd(data_dir) do
                BehaviouralAnalysis.get_inactive_trials(min_post_cuetime=0.2)
            end
            @test length(midx) == 20
            @test shash(midx) == 0xd0a56a94074e331b
            inactive_args = BehaviouralAnalysis.InactiveTrialsArgs(0.2)
            inactive_trials = cd(data_dir) do
                BehaviouralAnalysis.get_inactive_trials(inactive_args)
            end
            @test inactive_trials.midx == midx
        end
    end
end

@testset "Correct saccades" begin
    tdir = tempdir()
    data_dir = "James/20140904/session01/"
    cd(tdir) do
        mkpath(data_dir)
        Downloads.download("http://cortex.nus.edu.sg/testdata/eyetrials.bson", joinpath(data_dir, "eyetrials.bson"))
        Downloads.download("http://cortex.nus.edu.sg/testdata/event_data.mat", joinpath(data_dir, "event_data.mat"))
        cd(data_dir) do
            tidx, onset, offset, startpos, endpos = BehaviouralAnalysis.get_aborting_saccade(TrialsAnalysis.CorrectTrial)
            et = BSON.load("eyetrials.bson")
            eyetrials = convert(Vector{Stimulus.EyelinkTrial}, et[:eyetrials])
            Δt = [onset[i] - eyetrials[tidx[i]].response for i in 1:length(tidx)]
            @test all(Δt .> 0)
            correct_saccades = BehaviouralAnalysis.Saccades(BehaviouralAnalysis.SaccadesArgs(BehaviouralAnalysis.PlexonTimeReference(), BehaviouralAnalysis.CorrectSaccade()),force_redo=true, do_save=false)
            #TODO: How can we verify that these saccades are correct?
            @test length(correct_saccades.trialidx) == 473
            @test correct_saccades.trialidx[1] == 4
            @test correct_saccades.onset[1] ≈ 49643.02499999953
            @test correct_saccades.duration[1] ≈ 0.021999999999934516
            @test correct_saccades.angle[1] ≈ -2.6487781885675075
            ctrials, cidx = TrialsAnalysis.get_trials(BehaviouralAnalysis.CorrectSaccadeTrial)
            @test length(ctrials) == 473
            @test cidx[1:5] == [4, 6, 9, 10, 12]

            @testset "Reaction time" begin
                rtime = BehaviouralAnalysis.get_reaction_time()
                @test length(rtime) == length(ctrials)
                l,m,u = percentile(rtime, [25, 50, 75])
                @test l ≈ 120.22499999971478
                @test m ≈ 134.77499999850988
                @test u ≈ 155.27500000223517
                rtime = BehaviouralAnalysis.get_reaction_time(:nosuchsaccade)
                @test rtime == nothing
            end
        end
    end
end

@testset "Correct saccade angle" begin
    data_dir = joinpath(homedir(), "Documents", "research", "monkey", "newWorkingMemory", "James","20140807", "session01")
    tdir = tempdir()
    cd(tdir) do
        Downloads.download("http://cortex.nus.edu.sg/testdata/J20140807_sacacde_results_test.mat","saccade_results.mat")
        Downloads.download("http://cortex.nus.edu.sg/testdata/J20140807_event_data.mat","event_data.mat")
        saccade_angle = BehaviouralAnalysis.get_correct_saccade_angle()
        @test length(saccade_angle) == 3
        @test saccade_angle[1] ≈ 2.222999217376312
        @test saccade_angle[2] ≈ -2.9156976798062706
        @test saccade_angle[3] ≈ 2.5938741101544514
        rm("saccade_results.mat")
        rm("event_data.mat")
    end
end

@testset "Saccade type conversions" begin
    t1 = BehaviouralAnalysis.get_saccade_type(:correctsaccadetrial)
    s1 = Symbol(BehaviouralAnalysis.CorrectSaccade)
    @test t1 <: BehaviouralAnalysis.CorrectSaccade
    @test s1 == :correctsaccade
    t2 = BehaviouralAnalysis.get_saccade_type(:incorrectsaccadetrial)
    s2 = Symbol(BehaviouralAnalysis.IncorrectSaccade)

    @test t2 <: BehaviouralAnalysis.IncorrectSaccade
    @test s2 == :incorrectsaccade
    t3 = BehaviouralAnalysis.get_saccade_type(:abortedsaccadetrial)
    s3 = Symbol(BehaviouralAnalysis.AbortedSaccade)
    @test t3 <: BehaviouralAnalysis.AbortedSaccade
    @test s3 == :abortedsaccade

    t3 = BehaviouralAnalysis.get_saccade_type(:someunknowntype)
	@test t3 == nothing

    t3 = BehaviouralAnalysis.get_saccade_type(:jumptheguntrial)
    s3 = Symbol(BehaviouralAnalysis.PrematureSaccade)
	@test t3 <: BehaviouralAnalysis.PrematureSaccade
	@test s3 == :prematuresaccade

end

@testset "deg2pixels" begin
	settings = BehaviouralAnalysis.ExperimentSettings(Dict{String,Any}("screen_distance" => 57.0,
                                                           "screen_width" => 1920,
                                                           "screen_height" => 1200,
                                                           "screen_size" => 24*2.5),
                                                      BehaviouralAnalysis.ExperimentSettingsArgs())

	px = BehaviouralAnalysis.deg2pixels([1.0, 1.0], 57.0, (1920,1200), 24*2.5)
	@test px[1] ≈ 37.54111937019342
	@test px[2] ≈ 37.54111937019342
    px2 = BehaviouralAnalysis.deg2pixels([1.0, 1.0], settings)
    @test px2 ≈ px
end

@testset "ExperimentSettings" begin
	tdir = tempdir()
	cd(tdir) do
		pth = "Pancake/20130923/session01"
		mkpath(pth)
		cd(pth) do
			settings = BehaviouralAnalysis.ExperimentSettings(BehaviouralAnalysis.OldExperimentSettingsArgs())
			@test settings["screen_width"] == 1440
			@test settings["screen_height"] == 900
			@test settings["columns"] == 5
			@test settings["rows"] == 5
			targets = BehaviouralAnalysis.get_targets(settings)
			@test length(targets) == 25
			ii = findfirst(target->BehaviouralAnalysis.GeometryTypes.isinside(target, 921.4, 430.5), targets)
			@test ii == 18
		end
		rm("Pancake",recursive=true, force=true)
		mkpath("animal1/20200101/session01")
		cd("animal1/20200101/session01") do
			dd = Dict("screen_width" => 1920,
					  "screen_height" => 1200,
					  "screen_distance" => 67,
					  "screen_size" => 24,
					  "target_window_size" => 8.0,
					  "target_locations" => [[-10,-10],[10,-10], [-10,10], [10,10]],
                      "fixation_window" => 12.0)
			open("a010101_settings.txt","w") do ff
				BehaviouralAnalysis.JSON.print(ff, dd)
			end
			settings = BehaviouralAnalysis.ExperimentSettings(BehaviouralAnalysis.ExperimentSettingsArgs())
			@test settings["screen_width"] == 1920
			@test settings["screen_height"] == 1200
			@test settings["target_locations"] == [[-10,-10],[10,-10],[-10,10],[10,10]]
            rm("a010101_settings.txt")
			open("a1_1_1_settings.txt","w") do ff
				BehaviouralAnalysis.JSON.print(ff, dd)
			end
			@test settings["screen_width"] == 1920
			@test settings["screen_height"] == 1200
			@test settings["target_locations"] == [[-10,-10],[10,-10],[-10,10],[10,10]]
			saccade_pos = [1326.4 665.3 1315.7 583.6;955.3 942.0 194.7 219.2]
			locidx = BehaviouralAnalysis.toidx(saccade_pos, settings)
			@test locidx == [2,1,4,3]

            etrial = Stimulus.NewEyelinkTrial(1467.059, 1.1169999999999618, 0.0, 0.0, Stimulus.NewStimulus[Stimulus.NewStimulus(1.7260000000001128, 2.0260000000000673, 1, 3), Stimulus.NewStimulus(3.04300000000012, 3.344000000000051, 2, 2)], Eyelink.AlignedSaccade[Eyelink.AlignedSaccade(4.458000000000084, 4.505000000000109, 949.8f0, 574.0f0, 1326.4f0, 955.3f0, 15, :start)], 4.536000000000058, 4.27800000000002, 4.48700000000008, 5.287000000000035)
            @test BehaviouralAnalysis.istrialtype(etrial, TrialsAnalysis.DistractedTrial, settings)
            @test BehaviouralAnalysis.istrialtype(etrial, TrialsAnalysis.ConfusedTrial, settings) == false
            @test BehaviouralAnalysis.istrialtype(etrial, TrialsAnalysis.IncorrectOnStimTrial, settings)
            etrial = Stimulus.NewEyelinkTrial(1467.059, 1.1169999999999618, 0.0, 0.0, Stimulus.NewStimulus[Stimulus.NewStimulus(1.7260000000001128, 2.0260000000000673, 1, 2), Stimulus.NewStimulus(3.04300000000012, 3.344000000000051, 1, 3)], Eyelink.AlignedSaccade[Eyelink.AlignedSaccade(4.458000000000084, 4.505000000000109, 949.8f0, 574.0f0, 1326.4f0, 955.3f0, 15, :start)], 4.536000000000058, 4.27800000000002, 4.48700000000008, 5.287000000000035)
            @test BehaviouralAnalysis.istrialtype(etrial, TrialsAnalysis.DistractedTrial, settings) == false
            @test BehaviouralAnalysis.istrialtype(etrial, TrialsAnalysis.ConfusedTrial, settings) == true
            @test BehaviouralAnalysis.istrialtype(etrial, TrialsAnalysis.IncorrectOnStimTrial, settings)
			rm("a1_1_1_settings.txt")
		end
		rm("animal1",recursive=true, force=true)
	end
end

@testset "Error trials" begin
    tdir = tempdir()
    cd(tdir) do
        mkpath("Whiskey/20200106/session02")
        cd("Whiskey/20200106") do
            Downloads.download("http://cortex.nus.edu.sg/testdata/W20200106_event_markers.csv","event_markers.csv")
            cd("session02") do
                Downloads.download("http://cortex.nus.edu.sg/testdata/W20200106s02_eyetrials.bson","eyetrials.bson")
                Downloads.download("http://cortex.nus.edu.sg/testdata/w010602_settings.txt","w010602_settings.txt")
                dtrials, didx = TrialsAnalysis.get_trials(BehaviouralAnalysis.DistractedTrial)
                ctrials, cidx = TrialsAnalysis.get_trials(BehaviouralAnalysis.ConfusedTrial)
                inc_trials, iidx = TrialsAnalysis.get_trials(BehaviouralAnalysis.IncorrectOnStimTrial)
                @test length(didx) == 45
                @test length(cidx) == 57
                @test length(iidx) == 325
            end
        end
        rm("Whiskey",recursive=true, force=true)
    end
end
